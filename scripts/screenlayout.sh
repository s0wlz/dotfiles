#!/bin/sh

killall -q polybar

while pgrep -x polybar >/dev/null; do sleep 1; done

polybar -c $HOME/.config/polybar/config eDPbot &
polybar -c $HOME/.config/polybar/config eDPtop &

if xrandr --query | grep '\bconnected\b' | grep VGA
then
  polybar -c $HOME/.config/polybar/config VGAtop &
  if xrandr --query | grep '\bconnected\b' | grep HDMI
  then
    $HOME/gits/dotfiles/screenlayout/workplace.sh
  else
    $HOME/gits/dotfiles/screenlayout/home.sh
  fi
fi

if xrandr --query | grep '\bconnected\b' | grep HDMI
then
  polybar -c $HOME/.config/polybar/config HDMItop &
fi
