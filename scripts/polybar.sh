#!/bin/sh

killall -q polybar

while pgrep -x polybar >/dev/null; do sleep 1; done

polybar -c $HOME/.config/polybar/config eDPbot &
polybar -c $HOME/.config/polybar/config eDPtop &

if xrandr --query | grep '\bconnected\b' | grep VGA
then
  ##$HOME/.screenlayout/home.sh &
  polybar -c $HOME/.config/polybar/config VGAtop &
fi

if xrandr --query | grep '\bconnected\b' | grep HDMI
then
  ##$HOME/.screenlayout/home.sh &
  polybar -c $HOME/.config/polybar/config HDMItop &
fi
