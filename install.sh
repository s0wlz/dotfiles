#!/bin/sh

rm $XDG_CONFIG_HOME/bspwm
rm $XDG_CONFIG_HOME/dunst
rm $XDG_CONFIG_HOME/polybar
rm $XDG_CONFIG_HOME/rofi
rm $XDG_CONFIG_HOME/sxhkd
rm $XDG_CONFIG_HOME/termite
rm $XDG_CONFIG_HOME/vim
rm $XDG_CONFIG_HOME/compton.conf

ln -s $PWD/bspwm $XDG_CONFIG_HOME/bspwm
ln -s $PWD/dunst $XDG_CONFIG_HOME/dunst
ln -s $PWD/polybar $XDG_CONFIG_HOME/polybar
ln -s $PWD/rofi $XDG_CONFIG_HOME/rofi
ln -s $PWD/sxhkd $XDG_CONFIG_HOME/sxhkd
ln -s $PWD/termite $XDG_CONFIG_HOME/termite
ln -s $PWD/vim $XDG_CONFIG_HOME/vim
ln -s $PWD/compton.conf $XDG_CONFIG_HOME/compton.conf
