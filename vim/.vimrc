syntax		on
colorscheme Tomorrow-Night

set guifont=Inconsolata\ Nerd\ Font:h12
set number
set encoding=UTF-8

let mapleader="\\"

set hidden
set history=100

filetype indent on
set nowrap
set tabstop=4
set shiftwidth=4
set expandtab
set smartindent
set autoindent
set hlsearch
set showmatch

autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif

map <Esc>OP <F1>
map <Esc>OQ <F2>
map <Esc>OR <F3>
map <Esc>OS <F4>
map <Esc>[16~ <F5>
map <Esc>[17~ <F6>
map <Esc>[18~ <F7>
map <Esc>[19~ <F8>
map <Esc>[20~ <F9>
map <Esc>[21~ <F10>
map <Esc>[23~ <F11>
map <Esc>[24~ <F12>

nmap <silent><F2> :NERDTreeToggle<CR>
nmap <silent><F3> :NERDTreeFind<CR>

nmap <silent><Up> :wincmd k<CR>
nmap <silent><Down> :wincmd j<CR>
nmap <silent><Left> :wincmd h<CR>
nmap <silent><Right> :wincmd l<CR>

nmap <C-Left> :lefta vsplit<CR>
nmap <C-Right> :rightb vsplit<CR>
nmap <C-Up> :abo split<CR>
nmap <C-Down> :bel split<CR>

noremap <silent><Tab> :WSNext<CR>
noremap <silent><S-Tab> :WSPrev<CR>
noremap <silent><Leader><Tab> :WSClose<CR>
noremap <silent><Leader><S-Tab> :WSClose!<CR>
noremap <silent><C-t> :WSTabNew<CR>

cabbrev bonly WSBufOnly

let g:NERDTreeMapActivateNode="<F3>"
let g:NERDTreeMapPreview="<F4>"

let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#formatter = 'default'
let g:airline_powerline_fonts = 1
let g:airline_theme='bubblegum'

let g:workspace_powerline_separators = 1
let g:workspace_tab_icon = "\uf00a"
let g:workspace_left_trunc_icon = "\uf0a8"
let g:workspace_right_trunc_icon = "\uf0a9"

call plug#begin('~/.config/vim/plugs')
Plug 'vim-perl/vim-perl'
Plug 'mattn/vim-go'
Plug 'vim-latex/vim-latex'
Plug 'scrooloose/nerdcommenter'
Plug 'yegappan/mru'
Plug 'tpope/vim-surround'
Plug 'mox-mox/vim-starlite'
Plug 'scrooloose/nerdtree'
Plug 'Xuyuanp/nerdtree-git-plugin'
Plug 'bagrat/vim-workspace'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'ryanoasis/vim-devicons'
Plug 'nathanaelkane/vim-indent-guides'
Plug 'dhruvasagar/vim-table-mode'
Plug 'airblade/vim-gitgutter'
Plug 'tomtom/tcomment_vim'
Plug 'Valloric/YouCompleteMe'
call plug#end()

" Append modeline after last line in buffer.
" Use substitute() instead of printf() to handle '%%s' modeline in LaTeX
" files.
function! AppendModeline()
  let l:modeline = printf(" vim: set ts=%d sw=%d tw=%d %set :",
        \ &tabstop, &shiftwidth, &textwidth, &expandtab ? '' : 'no')
  let l:modeline = substitute(&commentstring, "%s", l:modeline, "")
  call append(line("$"), l:modeline)
endfunction
nnoremap <silent> <Leader>ml :call AppendModeline()<CR>
